//
//  MemesAppTests.swift
//  MemesAppTests
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//

import XCTest
@testable import MemesApp

class MemesAppTests: XCTestCase {
    var mockWebService:MockWebService!
    var mockViewController:MockViewController!
    var memesViewModel:MemesViewModel!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockWebService = MockWebService()
        mockViewController = MockViewController()
        memesViewModel = MemesViewModel()
        memesViewModel.delegate = mockViewController
        try super.setUpWithError()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        memesViewModel = nil
        mockWebService = nil
        mockViewController = nil
    }
    
    
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testGetMemeListDataWithSuccess() {
        let memeModel = Memes(success: true,
                              data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)])
        )
        mockWebService.reslt = .success(memeModel)
        memesViewModel.getMemeListData()
        XCTAssertEqual(mockViewController.memesViewModel.memesList!.count, 0)
    }
    
    func testGetMemeListDataWithFailure() {
        mockWebService.reslt = .failure(ApiExceptions.urlBad)
        mockWebService.reslt = .failure(ApiExceptions.noData)
        memesViewModel.getMemeListData()
        XCTAssertFalse(mockViewController.isFailedResponse)
    }
    
    func testGetMemeListDataWithDecodingErrorFailure() {
        //        let memeModel = Memes(success: false,
        //                              data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)]))
        mockWebService.reslt = .failure(ApiExceptions.decodingError)
        memesViewModel.getMemeListData()
        XCTAssertFalse(mockViewController.isFailedResponse)
    }
    
    func testGetMemeListDataWithDecodingMcokDataFailedErrorFailure() {
        mockWebService.reslt = .failure(ApiExceptions.mcokDataFailed)
        memesViewModel.getMemeListData()
        XCTAssertFalse(mockViewController.isFailedResponse)
    }
    
    func testGetMemeListDataWithDecodingApiFailedErrorFailure() {
        mockWebService.reslt = .failure(ApiExceptions.mcokDataFailed)
        //        mockWebService.reslt = .failure(ApiExceptions.apiFailed)
        memesViewModel.getMemeListData()
        XCTAssertFalse(mockViewController.isFailedResponse)
    }
    
    func testGetMemeListDataWithNil() {
        let memeModel = Memes(success: true, data:DataClass(memes:nil))
        mockWebService.reslt = .success(memeModel)
        memesViewModel.getMemeListData()
        XCTAssertNotNil(mockViewController.memesViewModel.memesList)
    }
    func testSearchWithNameMemeNil() {
        
        memesViewModel.searchListData(searchText: "Confsfs")
        XCTAssertEqual(mockViewController.memesViewModel.memesList!.count, 0)
    }
    
    
    func test_SearchWithIdMemeNil() {
        
        memesViewModel.searchListData(searchText: "2222")
        XCTAssertEqual(mockViewController.memesViewModel.memesList!.count, 0)
    }
    
    
    func testSearchWithNameMemeNotNil() {
        let memeModel = Memes(success: true,
                              data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)])
        )
        memesViewModel.memesList = memeModel.data.memes
        memesViewModel.searchListData(searchText: "Jack")
        XCTAssertEqual(mockViewController.memesViewModel.memesList!.count, 1)
    }
    
    
    func testSearchWithIdMemeNotNil() {
        let memeModel = Memes(success: true,
                              data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)])
        )
        memesViewModel.memesList = memeModel.data.memes
        memesViewModel.searchListData(searchText: "2222")
        XCTAssertEqual(mockViewController.memesViewModel.memesList!.count, 1)
    }
    
    func testDismissKeyboardTextIsEmpty() {
        let memeModel = Memes(success: true,
                              data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)])
        )
        memesViewModel.memesList = memeModel.data.memes
        memesViewModel.searchListData(searchText: "")
        XCTAssertTrue(mockViewController.isKeyboardDismmis)
    }
    
}

class MockWebService:WebServices {
    var reslt:Result<Memes?,Error> = .success(Memes(success: true,
                                                    data: DataClass(memes: [Meme(id: "1111", name: "Confusion", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300),Meme(id: "2222", name: "Jack on hight", url: "http://www.anyimage.com", width: 400, height: 400, boxCount: 300)])))
    override func getMemesService(completion: @escaping (Result<Memes?,Error>) -> ()) {
        
        completion(reslt)
    }
}

class MockViewController:MemeListViewModelDelegate{
    
    lazy var memesViewModel:MemesViewModel = MemesViewModel()
    var isFailedResponse  = false
    var isKeyboardDismmis = false
    
    func showListData(model: [Meme]?) {
        memesViewModel.memesList = model
    }
    
    func showFailureAlert(msg: String) {
        isFailedResponse = true;
    }
    
    func dismissSearchKeyboard() {
        isKeyboardDismmis = true
    }
}
