//
//  ImageStrings.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 30/08/22.
//

import Foundation
struct ImageStrings {
    static let placeholder = "placeholder"
}


struct ConstantUrl {
    static let memesUrl = "https://api.imgflip.com/get_memes"
}
