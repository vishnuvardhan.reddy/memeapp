//
//  ConstantStrings.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 30/08/22.
//

import Foundation
struct ConstantStrings {
    static let memeCellId = "MemeCell_ID"
    static let alert = "Alert"
    static let ok = "Ok"
    static let failedApiError = "failed to get Data from api"
    static let responceError = "Responce error"
    static let memeListisempty = "Meme List is empty"
    static let checkYourInternet = "Please check your internet connection"
    static let internet = "Internet"

}
