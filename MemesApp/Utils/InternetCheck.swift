//
//  InternetCheck.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 30/08/22.
//

import Foundation
import Reachability
class ConnectionCheck {
    
    static let shared = ConnectionCheck()    
    func hasConnectivity() -> Bool {
        do {
            let reachability: Reachability = try Reachability()
            let networkStatus = reachability.connection
            
            switch networkStatus {
            case .unavailable, .none:
                return false
            case .wifi, .cellular:
                return true
            }
        }
        catch {
            return false
        }
    }
}
