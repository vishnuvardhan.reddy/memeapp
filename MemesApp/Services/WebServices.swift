//
//  WebServices.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//

import Foundation

protocol WebServcieDelegate{
    func  getMemesService(completion:@escaping (Result<Memes?,Error>) ->())
}

class WebServices:WebServcieDelegate {
    func getMemesService(completion: @escaping (Result<Memes?,Error>) -> ()) {
        if  ConnectionCheck.shared.hasConnectivity() {
            let url = URL(string: ConstantUrl.memesUrl)!
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                
                guard let data = data, error == nil else {
                    completion(.failure(error ?? ApiExceptions.noData))
                    return
                }
                
                guard let results = try? JSONDecoder().decode(Memes.self, from: data) else {
                    completion(.failure(ApiExceptions.decodingError))
                    return
                }
                
                completion(.success(results))
            }.resume()
        }else{
            completion(.failure(ApiExceptions.mcokDataFailed))
            
        }
    }
}



//exceptions to catch, change as per your need
enum ApiExceptions: Error {
    case apiFailed
    case urlBad
    case mcokDataFailed
    case noData
    case decodingError
}
