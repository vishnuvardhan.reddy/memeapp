//
//  MemesViewModel.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//

import Foundation
protocol MemeListViewModelDelegate:AnyObject {
    func showListData(model:[Meme]?)
    func showFailureAlert(msg:String)
    func dismissSearchKeyboard()
}
protocol MemesViewModelProtocal {
    var memesList: [Meme]? { get set }
    func getMemeListData()
    func searchListData(searchText: String)
    
}
class MemesViewModel:MemesViewModelProtocal{
    
    var memesList: [Meme]?
    
    var webServcie: WebServices?
    
    var delegate: MemeListViewModelDelegate?
    
    init(service:WebServices = WebServices(),memeList:[Meme] = []) {
        webServcie = service
        memesList = memeList
    }
    
    func getMemeListData(){
        if  ConnectionCheck.shared.hasConnectivity() {
            webServcie?.getMemesService(completion: {  model   in
                
                switch model {
                case .success(let results):
                    if let memes = results?.data{
                        self.memesList = memes.memes
                    }
                    self.delegate?.showListData(model: self.memesList)
                case .failure(ApiExceptions.decodingError):
                    DispatchQueue.main.async { [weak self] in
                        self?.delegate?.showFailureAlert(msg: ConstantStrings.responceError)
                    }
                    break
                case .failure(ApiExceptions.urlBad), .failure(ApiExceptions.noData):
                    // present some generic network error message
                    // personally log these sorts of issues into Crashlytics, as this would suggest some programming bug, either in client app or in server, that DevOps or developers need to look into
                    DispatchQueue.main.async { [weak self] in
                        self?.delegate?.showFailureAlert(msg: ConstantStrings.failedApiError)
                    }
                    break
                case .failure(ApiExceptions.mcokDataFailed):
                    DispatchQueue.main.async { [weak self] in
                        self?.delegate?.showFailureAlert(msg: ConstantStrings.checkYourInternet)
                    }
                    break
                default: break
                    // present some generic network error message
                }
            })
            
        }
    }
    
    func searchListData(searchText text:String) {
        guard let memes = memesList else {
            return
        }
        if text.count > 3 {
            let numbersRange = text.rangeOfCharacter(from: .decimalDigits)
            let hasNumbers = (numbersRange != nil)
            if hasNumbers {
                let sortedMeme = memes.filter {$0.id.contains(text)}
                self.delegate?.showListData(model: sortedMeme)
            } else {
                let sortedMeme = memes.filter {$0.name.contains(text)}
                self.delegate?.showListData(model: sortedMeme)
            }
            
        } else if text.count == 0 {
            self.delegate?.showListData(model: memes)
            self.delegate?.dismissSearchKeyboard()
        }
    }
    
    
}

