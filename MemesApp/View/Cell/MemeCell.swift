//
//  MemeCell.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//

import UIKit

class MemeCell: UITableViewCell {
    
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
