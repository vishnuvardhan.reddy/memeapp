//
//  ViewController.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//

import UIKit
import AlamofireImage

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    lazy var memesViewModel:MemesViewModel = MemesViewModel()
    @IBOutlet weak var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        memesViewModel.getMemeListData()
        tableView.reloadData()
        
    }
}

//MARK: Alerts and MemeListViewModelDelegate functionlity
extension ViewController:MemeListViewModelDelegate{
    func showListData(model: [Meme]?) {
        DispatchQueue.main.async { [weak self] in
            self?.memesViewModel.memesList = model
            
            self?.tableView.reloadData()
        }
    }
    
    func showFailureAlert(msg: String) {
        let alert = UIAlertController(title: ConstantStrings.alert, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: ConstantStrings.ok, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func dismissSearchKeyboard() {
        self.searchBar.resignFirstResponder()
    }
    
}

//MARK: UITableView functionlity
extension ViewController :UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.memesViewModel.memesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConstantStrings.memeCellId, for: indexPath) as? MemeCell else {
            fatalError(ConstantStrings.failedApiError)
        }
        
        let meme = self.memesViewModel.memesList![indexPath.row]
        DispatchQueue.main.async {
            cell.name.text = meme.name
            cell.id.text = meme.id
            cell.count.text = "\(String(describing: meme.boxCount))"
            let url = URL(string: meme.url)!
            let placeholderImage = UIImage(named: ImageStrings.placeholder)
            
            cell.imgView.af_setImage(withURL: url, placeholderImage: placeholderImage)
            
        }
        return cell
    }
}

//MARK: Search bar delegate functionlity
extension ViewController : UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        return true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        memesViewModel.searchListData(searchText: searchText)
        
    }
}


