//
//  Memes.swift
//  MemesApp
//
//  Created by Vishnuvardhan Reddy on 25/08/22.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let memes = try? newJSONDecoder().decode(Memes.self, from: jsonData)

import Foundation

// MARK: - Memes
struct Memes: Codable {
    let success: Bool!
    let data: DataClass!
}

// MARK: - DataClass
struct DataClass: Codable {
    let memes: [Meme]!
}

// MARK: - Meme
struct Meme: Codable {
    let id, name: String!
    let url: String!
    let width, height, boxCount: Int!

    enum CodingKeys: String, CodingKey {
        case id, name, url, width, height
        case boxCount = "box_count"
    }
}
